#include <stdlib.h>
#include <stdio.h>

/*********************************
 * Funciones para abrir puerto serie
 * *****************************/
#include "libreria_serie.c"

#define PUERTO "/dev/ttyUSB0"
#define VELOCIDAD B9600
#define VELOCIDAD_ 9600
#define PARIDAD 0

main()
{
    int puerto_serie;
    int n;
    char buffer_memoria[1024];

    printf("Anbriendo puerto %s, velocidad %d, paridad %d...\n",PUERTO,VELOCIDAD_, PARIDAD);

    puerto_serie = abrir_puerto_serie(PUERTO, VELOCIDAD, PARIDAD);

    if(puerto_serie<0)
    {
        printf("ERROR al abrir puerto %s\n",PUERTO);
        exit(-1);
    }

    printf("Enviando caracter 'R'\n");
    
    write(puerto_serie, "R",1);

    // esperamos a que lleguen todos los caracteres de la respuesta, además el servidor tiene que consultar al sensor
    printf("Esperando respuesta 2 segundos...\n");
    sleep(2);

    n=read( puerto_serie, buffer_memoria, sizeof(buffer_memoria)-1);

    buffer_memoria[n]='\0'; // añado al final el caracter cero: fin de cadena

    printf("RESPUESTA: %s\n", buffer_memoria);

    close(puerto_serie);
}
