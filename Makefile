all: servidor cliente servidor_thread 

servidor: servidor.c printIP.c
	gcc servidor.c -o servidor

servidor_thread: servidor_thread.c
	gcc servidor_thread.c -o servidor_thread -pthread

cliente: cliente.c
	gcc cliente.c -o cliente

