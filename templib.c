#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TEMP_ERROR_FILE -1
#define TEMP_ERROR_CRC  -2
#define TEMP_ERROR_DATA -3
#define TEMP_OK          0

/********************************
 * FUNCIONES DISPONIBLES:
 * ******************************/


// Consulta el número de sensores encontrados en el sistema
int num_sensores();

// Consulta el nombre de fichero del sensor n-esimo
char * sensor_n(int n);

// Lee la temperatura del sensor con nombre de fichero file, obtenemos el
// resultado en temp
int lee_temp(const char *file, float *temp);



/********************************
 * Consulta el número de sensores disponible
 * ******************************/

int num_sensores()
{
    FILE *fp;
    int n;
    /* Open the command for reading. */
    fp = popen("ls /sys/bus/w1/devices/28-*/w1_slave | wc -l", "r");

    if (fp == NULL) {
        printf("Failed to run command\n" );
        return -1;
    }

    fscanf(fp," %d ", &n);

    /* close */
    pclose(fp);

    return n;
}

/********************************
 * Consulta el nombre del sensor # n
 * ******************************/
char * sensor_n(int n)
{
    FILE *fp;
    char fname[256];
    int i;
    /* Open the command for reading. */
    fp = popen("ls /sys/bus/w1/devices/28-*/w1_slave", "r");

    if (fp == NULL) {
        printf("Failed to run command\n" );
        return NULL;
    }
    i=0;

    while (i<n)   // lee hasta que llegue al que buscamos
    {
        fscanf(fp," %s ", fname);
        i++;
    }

    /* close */
    pclose(fp);

    return strdup(fname);   // devuelve un duplicado del nombre
}

/********************************
 * Consulta la temperatura del sensor con nombre file
 * ******************************/

int lee_temp(const char *file, float *temp)
{
    FILE *f;
    char ok[256];
    int t;
    int n;

    f=fopen(file,"r");

    if(f==NULL) return TEMP_ERROR_FILE;

    n=fscanf(f, " %*s %*s %*s %*s %*s %*s %*s %*s %*s : %*s %s ", ok);

    if(n!=1) return TEMP_ERROR_DATA;
    if(strcmp(ok,"YES")!=0) return TEMP_ERROR_CRC;

    n=fscanf(f, " %*s %*s %*s %*s %*s %*s %*s %*s %*s t=%d ",&t);

    if(n!=1) return TEMP_ERROR_DATA;

    fclose(f);

    *temp= (float)t/1000.0f;

    return TEMP_OK;
}

/***********************************
 * cowsays
 * ******************************/

int lavacadice(char * cadena, char * resultado)
{
	int i;
	int n;
	int c=0;
	char* inicio=resultado;
	n=strlen(cadena);
	resultado+=sprintf(resultado, "\n ");
	for(i=0; i< n+1; i++) resultado+=sprintf(resultado, "_");
	resultado+=sprintf(resultado, "\n< ");
	resultado+=sprintf(resultado, "%s",cadena);
	resultado+=sprintf(resultado, " >\n ");
	for(i=0; i< n+1; i++) resultado+=sprintf(resultado, "-");
	resultado+=sprintf(resultado, "\n");
	resultado+=sprintf(resultado, "        \\   ^__^\n");
	resultado+=sprintf(resultado, "         \\  (oo)\\_______\n");
	resultado+=sprintf(resultado, "            (__)\\       )\\/\\\n");
	resultado+=sprintf(resultado, "                ||----w |\n");
	resultado+=sprintf(resultado, "                ||     ||\n");
	return resultado-inicio;
}

void print_temperatura(float t)
{
	char buffer[1024];
	char cadena[256];
	sprintf(cadena, "%4.2f ºC", t);
	lavacadice(cadena,buffer);
	printf("%s",buffer);
}


