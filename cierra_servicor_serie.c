#include <stdlib.h>
#include <stdio.h>

#include <wiringSerial.h>


#define PUERTO "/dev/ttyUSB0"
#define VELOCIDAD 9600
#define RESPUESTA_SIZE 9

main()
{
    int file_descriptor;
    int n;
    char buffer_memoria[1024];

    printf("Anbriendo puerto %s, velocidad %d ...\n",PUERTO,VELOCIDAD);

    file_descriptor= serialOpen(PUERTO, VELOCIDAD);

    if(file_descriptor<0)
    {
        printf("ERROR al abrir puerto %s\n",PUERTO);
        exit(-1);
    }

    printf("Enviando caracter 'X'\n");
    serialPuts(file_descriptor, "X");

    printf("Esperando respuesta 1 segundo...\n");

    delay(1000);

    n=read( file_descriptor, buffer_memoria, RESPUESTA_SIZE);

    buffer_memoria[n]=0;

    if(n==RESPUESTA_SIZE)
    {
        printf("RESPUESTA: %s\n", buffer_memoria);
    }
    else
    {
        printf("ERROR respuesta tamaño no coincide: %d , '%s'\n",n, buffer_memoria);
    }

    serialClose(file_descriptor);
}
