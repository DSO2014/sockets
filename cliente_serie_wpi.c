#include <stdlib.h>
#include <stdio.h>

#include <wiringSerial.h>


#define PUERTO "/dev/ttyUSB0"
#define VELOCIDAD 9600

#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>

main()
{
    int puerto_serie;
    int n;
    char buffer_memoria[1024];

    printf("Anbriendo puerto %s, velocidad %d ...\n",PUERTO,VELOCIDAD);

    puerto_serie= serialOpen(PUERTO, VELOCIDAD);

    if(puerto_serie<0)
    {
        printf("ERROR al abrir puerto %s\n",PUERTO);
        exit(-1);
    }

    printf("Enviando caracter 'R'\n");
    serialPuts(puerto_serie, "R");

    // esperamos a que lleguen todos los caracteres de la respuesta, además el servidor tiene que consultar al sensor
    printf("Esperando respuesta 2 segundos...\n");
    delay(2000);

    n=read( puerto_serie, buffer_memoria, sizeof(buffer_memoria)-1);

    buffer_memoria[n]='\0'; // añado al final el caracter cero: fin de cadena

    printf("RESPUESTA: %s\n", buffer_memoria);

    serialClose(puerto_serie);
}
