#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

//--------------------------------------------------------------

void error(const char *msg)
{
    perror(msg);
    exit(1);
}
//--------------------------------------------------------------

int main(int argc, char *argv[])
{
    int sockfd = 0, buf_len = 0;
    char netbuf[1024], printbuf[257];
    struct sockaddr_in serv_addr;
    int puerto;

    if(argc != 4)
    {
        printf("\n Usar: %s <ip del server> <puerto> <mensaje>\n",argv[0]);
        return 1;
    }

    puerto=(int)strtol(argv[2],NULL,10);

    memset(netbuf, '0',sizeof(netbuf));
    if((sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_IP)) < 0)
    {
        printf("\n Error : Could not create socket \n");
        return 1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(puerto);

    if(inet_pton(AF_INET, argv[1], &serv_addr.sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
        return 1;
    }

    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\n Error : Connect Failed \n");
        return 1;
    }

    write(sockfd, argv[3], strlen(argv[3]));

    buf_len = read(sockfd, netbuf, sizeof(netbuf)-1);
    if (buf_len < 0)
        error("ERROR reading from socket");


    printf("cliente (%d) mensaje recivido. ",getpid() );
    printf(" IP: %s, puerto: %d\nMensaje-> ",
           inet_ntop(AF_INET,&(serv_addr.sin_addr),printbuf,256),ntohs(serv_addr.sin_port));

    fwrite(netbuf,sizeof(char), buf_len, stdout);
    printf("\n");

    close(sockfd);


    return 0;
}
