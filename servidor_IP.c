/****************************************************
 *
 * Ejemplo de servidor TCP/IP para acceso a sensor de temperatura
 * DAC 2015
 *
 * usar make para compilar
 * para listar puertos:     lsof -i
 *
 * *******************************************************/

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <pthread.h>


#include "printIP.c"


/*********************************
 * Librería de funciones para leer temperatura
 * *****************************/
#include "templib.c"

//--------------------------------------------------------------
void error(const char *msg)
{
    perror(msg);
    exit(1);
}



int  montar_direccion(struct sockaddr_in *direccion_servidor, int puerto)
{
    memset(direccion_servidor, '0', sizeof(*direccion_servidor));  // poner a cero
    direccion_servidor->sin_family = AF_INET; // IPv4
    direccion_servidor->sin_addr.s_addr = htonl(INADDR_ANY); // no sé mi dirección IP, escucho en cualquier interface
    direccion_servidor->sin_port = htons(puerto); // puerto
    return 0;
}


//--------------------------------------------------------------

char *nombre_sensor;  // fichero donde leer la temperatura

//--------------------------------------------------------------
/******************************************
 * Función que ejecutará el thread para atender una petición de servicio
 * ******************************************/
void* atiende( void *parametro)
{
    int bytes_escritos;
    char buffer_memoria[1025];
    int socket_conexion;
    int error_temp;
    float temperatura;

    socket_conexion=*((int*)parametro);

    error_temp = lee_temp(nombre_sensor, &temperatura);

    if(error_temp)
    {
        printf("Error de lectura en temperatura: %d\n",error_temp);
        bytes_escritos= sprintf(buffer_memoria,"ERROR CODE %d", error_temp);
    }
    else
    {
        printf("Thread enviando respuesta... Temperatura actual: %4.2f ºC\n",temperatura);
        bytes_escritos= sprintf(buffer_memoria,"%4.2f",temperatura);
    }

    write(socket_conexion, buffer_memoria, bytes_escritos); // escribe paquete
    close(socket_conexion);  // cierra conexion
    pthread_exit(NULL);
}

//--------------------------------------------------------------

int main(int argc, char *argv[])
{
    int socket_servidor = 0, socket_conexion = 0, direccion_cliente_size;
    struct sockaddr_in direccion_servidor, direccion_cliente;
    char buffer_memoria[256];
    int puerto;

    /*** comprueba argumentos ***/

    if(argc != 2)
    {
        printf("\n Usar: %s <puerto>\n",argv[0]);
        exit(-1);
    }

    if(sscanf(argv[1], "%d", &puerto)!=1)
    {
        printf("Puerto no válido\n Usar: %s <puerto>\n",argv[0]);
        exit(-1);
    }

    /****************
     * Inicia lectura temperatura
     * **********/

    if(num_sensores()==0)
    {
        printf("ERROR: No es posible leer temperaturas. Abortando...\n");
        exit(-2);
    }

    nombre_sensor = sensor_n(1);

    printf("Trabajaremos con el sensor: %s\n", nombre_sensor);

    /****************
     * Inicia red y abre el puerto para escuchar peticiones
     * **************/

    montar_direccion(&direccion_servidor, puerto);

    socket_servidor = socket(AF_INET /*IPv4*/, SOCK_STREAM /*TCP*/, IPPROTO_IP /*IP*/);
    if (socket_servidor<0) error("socket inicial");


    if( bind(socket_servidor, (struct sockaddr*)&direccion_servidor, sizeof(direccion_servidor)) )
        error("Bind");

    if ( listen(socket_servidor, 10) )
        error("Listen"); // escuchar y servir hasta 10 clientes simultáneos

    print_addresses(AF_INET); // imprime direcciones de los interfaces

    printf("*** servidor (%d) servicio escuchando en puerto %d\n", getpid(),puerto);

    /*************************************/

    pthread_t threads[20];
    unsigned long peticion=0;

    /*** atiende todas las peticiones que lleguen ***/

    while(1)
    {
        memset(&direccion_cliente, '0', sizeof(direccion_cliente));
        direccion_cliente_size = sizeof(direccion_cliente);
        socket_conexion = accept(socket_servidor, (struct sockaddr*) &direccion_cliente, &direccion_cliente_size); // esperar petición
        if(socket_conexion<0) error("socket al aceptar");

        printf("\n*** servidor (%d) llegó petición # %u\n",getpid(), peticion);
        printf("    Identificación cliente IP: %s, puerto: %d\n",
               inet_ntop(AF_INET,&(direccion_cliente.sin_addr),buffer_memoria,256), ntohs(direccion_cliente.sin_port));


        pthread_create(&threads[peticion%20], NULL, atiende, (void *) &socket_conexion );

        peticion++;

    }
}
//--------------------------------------------------------------
