#include <stdlib.h>
#include <stdio.h>

#include <wiringSerial.h>

/*********************************
 * Librería de funciones para leer temperatura
 * *****************************/
#include "templib.c"

#define PUERTO "/dev/ttyAMA0"
#define VELOCIDAD 9600

main()
{

    char c;
    int puerto_serie;
    int error_temp;
    float temperatura;
    char buffer_memoria[1024];
    char buffer_temp[1024];

    int longitud_respuesta;
    char * nombre_sensor;


    /****************
     * Inicia lectura temperatura
     * **********/

    if(num_sensores()==0)
    {
        printf("ERROR: No es posible leer temperaturas. Abortando...\n");
        exit(-2);
    }

    nombre_sensor = sensor_n(1);

    printf("Trabajaremos con el sensor: %s\n", nombre_sensor);

    printf("Escuchando en puerto %s, velocidad %d ...\n",PUERTO,VELOCIDAD);

    puerto_serie= serialOpen(PUERTO, VELOCIDAD);

    if(puerto_serie<0)
    {
        printf("ERROR al abrir puerto %s\n",PUERTO);
        exit(-1);
    }

    while(1)
    {
        read(puerto_serie, &c, 1); // lee un caracter

        if(c=='R')    // comando de lectura de temperatura
        {
            printf("Caracter 'R' recibido, enviando temperatura...\n");
            error_temp = lee_temp(nombre_sensor, &temperatura);

            if(error_temp)
            {
                printf("Error de lectura en temperatura: %d\n",error_temp);
                longitud_respuesta = sprintf(buffer_temp,"ERROR %d", error_temp);
            }
            else
            {
                printf("Temperatura actual: %4.2f ºC\n",temperatura);
                longitud_respuesta = sprintf(buffer_temp,"%4.2f ºC",temperatura);
            }
            longitud_respuesta= lavacadice(buffer_temp, buffer_memoria);
            write(puerto_serie, buffer_memoria, longitud_respuesta);
        }

        if(c=='X') // comando de terminación
        {
            printf("Caracter 'X' recibido, cerrando servidor...\n");
            longitud_respuesta = sprintf(buffer_memoria,"CERRANDO!");
            write(puerto_serie, buffer_memoria, longitud_respuesta);

            break;
        }
    }

    serialClose(puerto_serie);
}
