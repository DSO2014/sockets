// usar make para compilar
// para listar puertos:     lsof -i

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <pthread.h>


#include "printIP.c"

//--------------------------------------------------------------
void process_msg(char *b, int len)   // transforma a mayúsculas
{
    int	i;
    for(i =0; i < len; ++i)
        if(isalpha(* (b + i))) *(b+i) = toupper(*(b+i));
}
//--------------------------------------------------------------
void* atiende( void *p)
{
    int buf_len;
    char buf[1025];
    int connfd;

    connfd=*((int*)p);
    buf_len = read(connfd,buf,1024);  // lee paquet
    if (buf_len < 0)
    {
        error("ERROR reading from socket");
        exit(-1);
    }
    printf("thread servidor (%d) LLego %d bytes\n", getpid(),buf_len);
    process_msg(buf, buf_len); // hacer algún trabajo
    write(connfd, buf, buf_len); // escribe paquete
    close(connfd);  // cierra conexion
    pthread_exit(NULL);
}

//--------------------------------------------------------------

int main(int argc, char *argv[])
{
    int forkpid, listenfd = 0, connfd = 0, buf_len, cli_len;
    struct sockaddr_in serv_addr, cli_addr;
    int puerto;

    char buf[1025], buffer[257];

    if(argc != 2)
    {
        printf("\n Usar: %s <puerto>\n",argv[0]);
        return 1;
    }
    puerto=(int)strtol(argv[1],NULL,10);



    listenfd = socket(AF_INET /*IPv4*/, SOCK_STREAM /*TCP*/, IPPROTO_IP /*IP*/);

    memset(&serv_addr, '0', sizeof(serv_addr));  // poner a cero
    serv_addr.sin_family = AF_INET; // IPv4
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY); // no sé mi dirección IP
    serv_addr.sin_port = htons(puerto); // puerto

    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

    listen(listenfd, 10); // escuchar y servir hasta 10 clientes simultáneos

    print_addresses(AF_INET); // pinta direcciones de los interfaces

    printf("\n(%d) servicio progr. con threads escuchando en puerto %d\n", getpid(),puerto);

    pthread_t tid;

    while(1)
    {
        memset(&cli_addr, '0', sizeof(cli_addr));
        cli_len=sizeof(cli_addr);

        connfd = accept(listenfd, (struct sockaddr*) &cli_addr, &cli_len); // esperar petición

        pthread_create(&tid , NULL, atiende, (void *) &connfd );

        printf("servidor (%d) petición antendida\n", getpid());
    }
}
//--------------------------------------------------------------
