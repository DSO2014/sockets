#include <stdlib.h>
#include <stdio.h>

/*********************************
 * Funciones para abrir puerto serie
 * *****************************/
#include "libreria_serie.c"

/*********************************
 * Funciones para leer temperatura
 * *****************************/
#include "templib.c"

#define PUERTO "/dev/ttyAMA0"
#define VELOCIDAD B9600
#define VELOCIDAD_ 9600
#define PARIDAD 0

main()
{
    char c;
    int puerto_serie;
    int error_temp;
    float temperatura;
    char buffer_memoria[1024];
    char buffer_temp[1024];

    int longitud_respuesta;
    char * nombre_sensor;


    /****************
     * Inicia lectura temperatura
     * **********/

    if(num_sensores()==0)
    {
        printf("ERROR: No es posible leer temperaturas. Abortando...\n");
        exit(-2);
    }

    nombre_sensor = sensor_n(1);

    printf("Trabajaremos con el sensor: %s\n", nombre_sensor);

    printf("Escuchando puerto %s, velocidad %d, paridad %d...\n",PUERTO,VELOCIDAD_, PARIDAD);

    puerto_serie = abrir_puerto_serie(PUERTO, VELOCIDAD, PARIDAD);

    if(puerto_serie<0)
    {
        printf("ERROR al abrir puerto %s\n",PUERTO);
        exit(-1);
    }

    while(1)
    {

        read(puerto_serie, &c, 1); // lee un caracter

        if(c=='R')    // comando de lectura de temperatura
        {
            printf("Caracter 'R' recibido, enviando temperatura...\n");
            error_temp = lee_temp(nombre_sensor, &temperatura);

            if(error_temp)
            {
                printf("Error de lectura en temperatura: %d\n",error_temp);
                longitud_respuesta = sprintf(buffer_temp,"ERROR %d", error_temp);
            }
            else
            {
                printf("Temperatura actual: %4.2f ºC\n",temperatura);
                longitud_respuesta = sprintf(buffer_temp,"%4.2f ºC",temperatura);
            }
            longitud_respuesta= lavacadice(buffer_temp, buffer_memoria);
            write(puerto_serie, buffer_memoria, longitud_respuesta);
        }

        if(c=='X') // comando de terminación
        {
            printf("Caracter 'X' recibido, cerrando servidor...\n");
            longitud_respuesta = sprintf(buffer_memoria,"CERRANDO!");
            write(puerto_serie, buffer_memoria, longitud_respuesta);

            break;
        }
    }

    close(puerto_serie);
}
