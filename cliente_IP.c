#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

//--------------------------------------------------------------

void error(const char *msg)
{
    perror(msg);
    exit(1);
}


int  montar_direccion(struct sockaddr_in *direccion_servidor, char *IP, int puerto)
{
    memset(direccion_servidor, '0', sizeof(*direccion_servidor));  // poner a cero
    direccion_servidor->sin_family = AF_INET;
    direccion_servidor->sin_port = htons(puerto);

    if(inet_pton(AF_INET, IP, &direccion_servidor->sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
        exit(-1);
    }
    return 0;
}

//--------------------------------------------------------------

int main(int argc, char *argv[])
{
    int socket_conexion = 0, bytes_leidos = 0;
    char buffer_memoria[1024];
    float temperatura;
    struct sockaddr_in direccion_servidor;
    char *direccion_IP;
    int puerto;

    /*** comprueba argumentos ***/

    if(argc != 3)
    {
        printf("\n Usar: %s <ip del server> <puerto> \n",argv[0]);
        return 1;
    }

    if(sscanf(argv[2], "%d", &puerto)!=1)
    {
        printf("Puerto no válido\n Usar: %s <puerto>\n",argv[0]);
        exit(-1);
    }

    direccion_IP = argv[1];

    /***************
     * prepara conexión con el servidor
     * ************/


    if((socket_conexion = socket(AF_INET, SOCK_STREAM, IPPROTO_IP)) < 0)
    {
        printf("\n Error : Could not create socket \n");
        return 1;
    }

    montar_direccion(&direccion_servidor, direccion_IP, puerto);

    /*** conecta con el servidor ***/

    if( connect(socket_conexion, (struct sockaddr *) &direccion_servidor, sizeof(direccion_servidor)) < 0)
    {
        printf("\n Error : Connect Failed \n");
        return 1;
    }

    /*** recive la temperatura ***/
    bytes_leidos = read(socket_conexion, buffer_memoria, sizeof(buffer_memoria)-1);
    if (bytes_leidos < 0)
        error("ERROR reading from socket");

    if(sscanf(buffer_memoria, "%f", &temperatura)!=1) error("ERROR reading temperature from buffer");

    /*** pinta el resultado ***/

    printf("cliente (%d) respuesta recibida desde: ",getpid() );
    printf(" IP: %s, puerto: %d\n Temperatura = ",
           inet_ntop(AF_INET,&(direccion_servidor.sin_addr),buffer_memoria,256), ntohs(direccion_servidor.sin_port));

    printf("%4.2f ºC\n", temperatura);

    close(socket_conexion);


    return 0;
}
