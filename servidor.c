// usar make para compilar
// para listar puertos:     lsof -i

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>

#include "printIP.c"

//--------------------------------------------------------------
void  maneja(int s)  // recive cualquier hijo SIGCHLD
{
    int status, wpid;
    wpid=wait(&status);
    printf("\n*** servidor (%d) fin proceso hijo pid %d\n", getpid(), wpid);
}

//--------------------------------------------------------------
void process_msg(char *b, int len)   // transforma a mayúsculas
{
    int	i;
    for(i =0; i < len; ++i)
        if(isalpha(* (b + i))) *(b+i) = toupper(*(b+i));
}
//--------------------------------------------------------------
void error(const char *msg)
{
    perror(msg);
    exit(1);
}
//--------------------------------------------------------------

int main(int argc, char *argv[])
{
    int forkpid, listenfd = 0, connfd = 0, buf_len, cli_len;
    struct sockaddr_in serv_addr, cli_addr;
    int puerto;

    char netbuf[1025], printbuf[257];

    if(argc != 2)
    {
        printf("\n Usar: %s <puerto>\n",argv[0]);
        return 1;
    }
    puerto=(int)strtol(argv[1],NULL,10);


    signal(SIGCHLD,maneja);

    listenfd = socket(AF_INET /*IPv4*/, SOCK_STREAM /*TCP*/, IPPROTO_IP /*IP*/);
    if (listenfd<0) error("socket inicial");

    memset(&serv_addr, '0', sizeof(serv_addr));  // poner a cero
    serv_addr.sin_family = AF_INET; // IPv4
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY); // no sé mi dirección IP
    serv_addr.sin_port = htons(puerto); // puerto

    if( bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) )
        error("Bind");

    if ( listen(listenfd, 10) )
        error("Listen"); // escuchar y servir hasta 10 clientes simultáneos

    print_addresses(AF_INET); // pinta direcciones de los interfaces

    printf("*** servidor (%d) servicio escuchando en puerto %d\n", getpid(),puerto);

    while(1) // el servidor nunca termina
    {
        memset(&cli_addr, '0', sizeof(cli_addr));
        cli_len=sizeof(cli_addr);

        connfd = accept(listenfd, (struct sockaddr*) &cli_addr, &cli_len); // esperar petición
        if(connfd<0) error("socket al aceptar");
        printf("*** servidor (%d) llegó petición\n",getpid());
        forkpid=fork(); // crear hijo para atender petición

        if(forkpid==0)  // soy el hijo
        {
            close(listenfd);
            buf_len = read(connfd,netbuf,1024);  // lee paquete
            if (buf_len < 0) error("ERROR lectura socket");
            printf("*** hijo servidor (%d) LLegaron %d bytes desde ", getpid(),buf_len);
            printf(" IP: %s, puerto: %d\n*** Mensaje-> ",
                   inet_ntop(AF_INET,&(cli_addr.sin_addr),printbuf,256),ntohs(cli_addr.sin_port));
            fwrite(netbuf,sizeof(char), buf_len, stdout);
            printf("\n");


            process_msg(netbuf, buf_len); // hacer algún trabajo

            write(connfd, netbuf, buf_len); // escribe paquete
            close(connfd);  // cierra conexion
            exit(0);
        }
        // continua el padre
        close(connfd);
        printf("*** servidor (%d) petición antendida\n", getpid());
    }
}
//--------------------------------------------------------------
